try: paraview.simple
except: from paraview.simple import *

from paraview import coprocessing

#--------------------------------------------------------------
# Code generated from cpstate.py to create the CoProcessor.


# ----------------------- CoProcessor definition -----------------------

def CreateCoProcessor():
  def _CreatePipeline(coprocessor, datadescription):
    class Pipeline:

      pvgrid = coprocessor.CreateProducer(datadescription, "input")

      # current camera placement for RenderView1
      RenderView1 = coprocessor.CreateView(CreateRenderView, "results/image%t.png", 1, 0, 1, 1384, 932)
      RenderView1.CameraPosition = [0.049999999813735485, 0.049999999813735485, 0.4625884990668317]
      RenderView1.CameraFocalPoint = [0.049999999813735485, 0.049999999813735485, 0.05]
      RenderView1.CameraParallelScale = 0.10678576134875965

      # write data
      ParallelUnstructuredGridWriter = coprocessor.CreateWriter(XMLPUnstructuredGridWriter, "results/pvgrid%t.pvtu", 1)

      # show data in view
      DataRepresentation = Show(pvgrid, RenderView1, 'UnstructuredGridRepresentation')

      # trace defaults for the display properties.
      DataRepresentation.Representation = 'Surface'
      DataRepresentation.ColorArrayName = [None, '']
      DataRepresentation.SelectTCoordArray = 'None'
      DataRepresentation.SelectNormalArray = 'None'
      DataRepresentation.SelectTangentArray = 'None'
      DataRepresentation.OSPRayScaleArray = 'State'
      DataRepresentation.OSPRayScaleFunction = 'PiecewiseFunction'
      DataRepresentation.SelectOrientationVectors = 'None'
      DataRepresentation.ScaleFactor = 0.010000000000000002
      DataRepresentation.SelectScaleArray = 'None'
      DataRepresentation.GlyphType = 'Arrow'
      DataRepresentation.GlyphTableIndexArray = 'None'
      DataRepresentation.GaussianRadius = 0.0005
      DataRepresentation.SetScaleArray = ['POINTS', 'State']
      DataRepresentation.ScaleTransferFunction = 'PiecewiseFunction'
      DataRepresentation.OpacityArray = ['POINTS', 'State']
      DataRepresentation.OpacityTransferFunction = 'PiecewiseFunction'
      DataRepresentation.DataAxesGrid = 'GridAxesRepresentation'
      DataRepresentation.PolarAxes = 'PolarAxesRepresentation'
      DataRepresentation.ScalarOpacityUnitDistance = 0.008660254037844388
      DataRepresentation.OpacityArrayName = ['POINTS', 'State']

      # init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
      DataRepresentation.ScaleTransferFunction.Points = [0.9819998431955903, 0.0, 0.5, 0.0, 1.0081045959722779, 1.0, 0.5, 0.0]

      # init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
      DataRepresentation.OpacityTransferFunction.Points = [0.9819998431955903, 0.0, 0.5, 0.0, 1.0081045959722779, 1.0, 0.5, 0.0]

      # reset view to fit data
      RenderView1.ResetCamera(False)

      # get the material library
      materialLibrary1 = GetMaterialLibrary()

      # rename source object
      RenameSource('input', pvgrid)

      # update the view to ensure updated data information
      RenderView1.Update()

      # set scalar coloring
      ColorBy(DataRepresentation, ('POINTS', 'State', '0'))

      # rescale color and/or opacity maps used to include current data range
      DataRepresentation.RescaleTransferFunctionToDataRange(True, False)

      # show color bar/color legend
      DataRepresentation.SetScalarBarVisibility(RenderView1, True)

      # get color transfer function/color map for 'State'
      stateLUT = GetColorTransferFunction('State')

      # get opacity transfer function/opacity map for 'State'
      statePWF = GetOpacityTransferFunction('State')

      # create a new 'Slice'
      slice1 = Slice(registrationName='Slice1', Input=pvgrid)
      slice1.SliceType = 'Plane'
      slice1.HyperTreeGridSlicer = 'Plane'
      slice1.SliceOffsetValues = [0.0]

      # init the 'Plane' selected for 'SliceType'
      slice1.SliceType.Origin = [0.05, 0.05, 0.05]

      # init the 'Plane' selected for 'HyperTreeGridSlicer'
      slice1.HyperTreeGridSlicer.Origin = [0.05, 0.05, 0.05]

      # Properties modified on slice1.SliceType
      slice1.SliceType.Normal = [0.0, 0.0, 1.0]

      # show data in view
      slice1Display = Show(slice1, RenderView1, 'GeometryRepresentation')

      # trace defaults for the display properties.
      slice1Display.Representation = 'Surface'
      slice1Display.ColorArrayName = ['POINTS', 'State']
      slice1Display.LookupTable = stateLUT
      slice1Display.SelectTCoordArray = 'None'
      slice1Display.SelectNormalArray = 'None'
      slice1Display.SelectTangentArray = 'None'
      slice1Display.OSPRayScaleArray = 'State'
      slice1Display.OSPRayScaleFunction = 'PiecewiseFunction'
      slice1Display.SelectOrientationVectors = 'None'
      slice1Display.ScaleFactor = 0.010000000000000002
      slice1Display.SelectScaleArray = 'None'
      slice1Display.GlyphType = 'Arrow'
      slice1Display.GlyphTableIndexArray = 'None'
      slice1Display.GaussianRadius = 0.0005
      slice1Display.SetScaleArray = ['POINTS', 'State']
      slice1Display.ScaleTransferFunction = 'PiecewiseFunction'
      slice1Display.OpacityArray = ['POINTS', 'State']
      slice1Display.OpacityTransferFunction = 'PiecewiseFunction'
      slice1Display.DataAxesGrid = 'GridAxesRepresentation'
      slice1Display.PolarAxes = 'PolarAxesRepresentation'

      # init the 'PiecewiseFunction' selected for 'ScaleTransferFunction'
      slice1Display.ScaleTransferFunction.Points = [0.9819998431955905, 0.0, 0.5, 0.0, 1.0081045959722776, 1.0, 0.5, 0.0]

      # init the 'PiecewiseFunction' selected for 'OpacityTransferFunction'
      slice1Display.OpacityTransferFunction.Points = [0.9819998431955905, 0.0, 0.5, 0.0, 1.0081045959722776, 1.0, 0.5, 0.0]

      # hide data in view
      Hide(pvgrid, RenderView1)

      # show color bar/color legend
      slice1Display.SetScalarBarVisibility(RenderView1, True)

      # update the view to ensure updated data information
      RenderView1.Update()

      # reset view to fit data
      RenderView1.ResetCamera(False)

    return Pipeline()

  class CoProcessor(coprocessing.CoProcessor):
    def CreatePipeline(self, datadescription):
      self.Pipeline = _CreatePipeline(self, datadescription)

  coprocessor = CoProcessor()
  freqs = {'input': [1]}
  coprocessor.SetUpdateFrequencies(freqs)
  return coprocessor

#--------------------------------------------------------------
# Global variables that will hold the pipeline for each timestep
# Creating the CoProcessor object, doesn't actually create the ParaView pipeline.
# It will be automatically setup when coprocessor.UpdateProducers() is called the
# first time.
coprocessor = CreateCoProcessor()

#--------------------------------------------------------------
# Enable Live-Visualizaton with ParaView
coprocessor.EnableLiveVisualization(True)


# ---------------------- Data Selection method ----------------------

def RequestDataDescription(datadescription):
    "Callback to populate the request for current timestep"
    global coprocessor
    if datadescription.GetForceOutput() == True:
        # We are just going to request all fields and meshes from the simulation
        # code/adaptor.
        for i in range(datadescription.GetNumberOfInputDescriptions()):
            datadescription.GetInputDescription(i).AllFieldsOn()
            datadescription.GetInputDescription(i).GenerateMeshOn()
        return

    # setup requests for all inputs based on the requirements of the
    # pipeline.
    coprocessor.LoadRequestedData(datadescription)

# # ------------------------ Processing method ------------------------

def DoCoProcessing(datadescription):
    "Callback to do co-processing for current timestep"
    global coprocessor

    # Update the coprocessor by providing it the newly generated simulation data.
    # If the pipeline hasn't been setup yet, this will setup the pipeline.
    coprocessor.UpdateProducers(datadescription)

    # Write output data, if appropriate.
    coprocessor.WriteData(datadescription)

    # Write image capture (Last arg: rescale lookup table), if appropriate.
    # coprocessor.WriteImages(datadescription, rescale_lookuptable=False)

    # Live Visualization, if enabled.
    coprocessor.DoLiveVisualization(datadescription, "localhost", 22221)
