#!/bin/sh

conda create -n PARTITION -y -c conda-forge python=3.9 sphinx=5.3.0
conda activate PARTITION
cd docs
make clean
make html
conda deactivate
conda remove -n PARTITION --all -y

printf '\nDocumentation is available at docs/build/html/index.html\n\n'