# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('../..'))


# -- Project information -----------------------------------------------------

project = 'PARTITION'
copyright = '2022-2023, CERFACS'
author = 'Zoltan Csati'

# The full version, including alpha/beta/rc tags
release = '1.0.0'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.inheritance_diagram',
    'sphinx.ext.mathjax',
    'sphinx.ext.napoleon',
    'sphinx.ext.viewcode',
    'sphinx.ext.intersphinx',
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['requirements.txt',
                    'modules.rst'        # autodoc generates it automatically, but we
                                         # rather create our own API structure in api.rst
                    ]

# Explicitly define the main file (https://stackoverflow.com/a/56448499/4892892)
# so that it works with older Sphinx versions as well
# https://www.sphinx-doc.org/en/master/usage/configuration.html#confval-master_doc
master_doc = 'index'

# Do not prepend the "partition" module name to the object names
add_module_names = False

# Display all warning messages...
nitpicky = True

# ... except the ones of which I know that they are the safe to be ignored
nitpick_ignore = [('py:class', 'vtkmodules.vtkCommonDataModel.vtkUnstructuredGrid')]  # VTK is Doxygen-based, so no way that Intersphinx can find it

# Napoleon settings
napoleon_google_docstring = False
napoleon_include_private_with_doc = True
napoleon_use_param = False
napoleon_use_rtype = False

# autodoc settings
autodoc_mock_imports = ['mpi4py', 'numpy', 'paraview', 'vtkmodules']
autodoc_member_order = 'bysource'  # https://stackoverflow.com/q/37209921/4892892

# intersphinx settings
intersphinx_mapping = {'python': ('https://docs.python.org/3', None),
                       'numpy': ('https://numpy.org/doc/stable/', None),
                       'sphinx': ('https://www.sphinx-doc.org/en/master/', None)}



# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = 'nature'
