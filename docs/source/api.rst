#########
PARTITION
#########

.. currentmodule:: partition
.. automodule:: partition
   :members:
   :undoc-members:
   :show-inheritance:
