# PARTITION

PARallel in-siTu vIsualizaTIOn miNiapp



## Dependencies

The serial version of the miniapp requires Python 3.9 or later, VTK and ParaView.
The parallel version additionally needs `mpi4py` and a working MPI installation.
If you downloaded the pre-packaged version of ParaView, it comes with Python, VTK, mpi4py and MPI,
so all the required dependencies are met.

For [generating the documentation](#building-the-documentation), you will need Python 3.9 or later and Sphinx.



## Usage

The miniapp can be used both in [batch mode](#batch-processing) and in [interactive mode](#live-visualization).
During batch processing, the requested data processing, defined in *ScriptCoProcess.py*, is performed in-situ and the results are saved into *results/*.
This execution mode is recommended for production use.
However, when experimenting with a new simulation, the live visualization provides insight.


### Batch processing

You can run the miniapp from the root directory in serial mode with

```bash
pvpython partition.py <options> ScriptCoProcess.py
```

or in parallel with

```bash
mpiexec -n <n_process> pvbatch --sym --mpi partition.py <options> ScriptCoProcess.py
```
replacing `<n_process>` with the number of MPI processes you want to launch.
The options are shown in the command line when `partition` is called with the `-h` flag:

```bash
pvpython partition.py -h
```

### Live visualization

To see the evolving simulation in real-time, you must
1. Launch the ParaView server(s), which will receive and process the simulation data

   In what follows, we assume that the data server -- render server configuration is used.
   If you want a single server, replace `pvdataserver` and `pvrenderserver` with `pvserver`.

   In serial mode, fire up the ParaView servers in two terminals. You will see something like this:
   ```bash
   $ pvdataserver
   Waiting for client...
   Connection URL: cdsrs://trevor.cerfacs.fr:11111/<render-server-hostname>:22221
   Accepting connection(s): trevor.cerfacs.fr:11111
   ```
   and
   ```bash
   $ pvrenderserver
   Waiting for client...
   Connection URL: cdsrs://<data-server-hostname>:11111/trevor.cerfacs.fr:22221
   Accepting connection(s): trevor.cerfacs.fr:22221
   ```

   The fake simulation in PARTITION uses domain decomposition to mimic a real-world parallel code.
   Most data processing pipelines in ParaView can also benefit from domain decomposition.
   Therefore, you can launch the ParaView servers on multiple MPI processes, e.g.
   ```bash
   mpiexec -n 4 pvdataserver
   ```


2. Open the ParaView GUI

3. Connect the GUI with the servers

   Once opened, click on *File* -> *Connect...* and set the host names and port numbers shown after "Accepting connection(s):" of Step 1.
   You can ensure that the connection is established by

   - enabling *View* -> *Memory Inspector*
   - looking at the terminal, which will display "Client connected."

4. Start Catalyst, which will listen to the simulation code.

   Click on *Catalyst* -> *Connect...* and set the port number to 22221.
   Then *Catalyst* -> *Pause Simulation* to suspend the execution of the upcoming simulation (step 5).

5. Launch the simulation
   
   Use the guidelines in the section [Batch processing](#batch-processing).
   If you invoked *partition.py* in parallel, it makes sense to launch multiple ParaView data servers too.
   Use the `-d` flag in the PARTITION command line interface to display verbose information in the terminal as the simulation runs.

6. Inspect the simulation in the ParaView GUI, possibly pausing and resuming the execution.

   Since you paused the simulation from the GUI in the step 4, the full simulation data and the derived data (here, a slice of the whole domain) can be fetched (*View* -> *Pipeline Browser*).
   When you decide to resume the simulation, select *Catalyst* -> *Continue*.
   You can also set a breakpoint for a later time instance: *Catalyst* -> *Set Breakpoint*.



## Experiment with the code

PARTITION is open-source.
Feel free to play with it and make modifications.
Here are a few examples worth trying ([suggest others](#contributing)).

- Modify the *ScriptCoProcess.py* script to define other visualization pipelines.
- Monitor the usage of resources (CPU, RAM) and the parallel scalability as a function of the number of cells (flag `-c`) and the number of MPI processes.
- See what happens when increasing the order of the hexahedral cells (`-o`).
Currently, orders 1, 2 and 3 are implemented, but it is easy to implement higher orders because the code was written with this in mind.
- Add support for other cell types (tetrahedron, for example).
- When in [live visualization mode](#live-visualization)
   - try out different display modes. E.g. *Surface With Edges* to display the cells, or *Points* to see the equidistant points in the VTK Lagrange cells.
     These display modes are instructive when modifying the subdivision level.
   - investigate the effect of the subdivision of the VTK Lagrange cells (can be set in the *Adaptive Subdivision Level* option) on the approximation of the geometry and the field value. For the geometry, use the tube domain by passing `-g tube` in the command line interface.
   - see how the number of cells (`-c n1 n2 n3`) influences the approximations. As the number of cells increases, the linear approximation (*Adaptive Subdivision Level*: 1) produces better and better results, implying that the VTK Lagrange cells are mostly beneficial for coarse meshes.



## Contributing

You are more than welcome to contribute!
This can include
- suggesting ideas or concrete implementations of interesting features
- bugs you found
- improving the code quality and the documentation



## Building the documentation

To create the automatically generated documentation for PARTITION, all you need is [Sphinx](https://www.sphinx-doc.org/en/master/) (with no external extension).
If you do not have Sphinx installed, it is recommended to install it into a virtual environment.
In case of [Conda](https://docs.conda.io/en/latest/miniconda.html), the virtual environment and the installation can simply be done with the following one-liner:
```bash
conda create -n PARTITION -y -c conda-forge python=3.9 sphinx=5.3.0
```
Activate the new environment:
```bash
conda activate PARTITION
```
Now you can build the documentation from the *docs/* directory with
```bash
make html
```
Open *docs/build/html/index.html* in a web browser to see the generated output.

If you do not wish to compile the documentation anymore, you can remove the virtual environment:
```bash
conda deactivate
conda remove -n PARTITION --all -y
```

For a one-time building, these steps are collected in the *build_docs.sh* script:
```bash
source build_docs.sh
```



## Behind the name

Domain decomposition/partitioning is central to distributed parallelism, hence the name.
PARTITION stands for PARallel in-siTu vIsualizaTIOn miNiapp.
The acronym was generated by [Acronymify!](https://acronymify.com/PARTITION/?q=parallel+in-situ+visualization+miniapp)
